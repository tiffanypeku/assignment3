Game.create!(id:0) if Game.count.eql?0
Player.create!([ {id: 0,name: "Jon", value: "X",game_id: 0}, {id: 1,name: "Bob", value: "O",game_id: 0} ]) if Player.count.eql?0
if Cell.count.eql?0
  (1..9).each do |n|
    case n
    when 1
      Cell.create!(x:0,y:0)
    when 2
      Cell.create!(x:0,y:1)
    when 3
      Cell.create!(x:0,y:2)
    when 4
      Cell.create!(x:1,y:0)
    when 5
      Cell.create!(x:1,y:1)
    when 6
      Cell.create!(x:1,y:2)
    when 7
      Cell.create!(x:2,y:0)
    when 8
      Cell.create!(x:2,y:1)
    when 9
      Cell.create!(x:2,y:2)
    end
  end
end
