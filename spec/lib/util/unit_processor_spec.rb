require 'rails_helper'

module Util
  RSpec.describe do
    # TODO: change the name
    let(:including_class) { Class.new { include Util } }
    let(:input) { '1 celsius to fahrenheit' }
    let(:result_for_valid_input) { 1.celsius.to.fahrenheit.value }
    let(:invalid_input_length) { '1 celsius' }
    let(:invalid_input_no_number) { '123s celsius' }
    let(:invalid_input_from_unit) { '1 celcius to fahrenheit' }
    let(:invalid_input_to_unit) { '1 celcius to farhenheit' }
    let(:fail_conversion_message) { FAIL_CONVERSION_MESSAGE }

    describe '#calculate' do
      context 'if input successfully converted' do
        it 'returns the value of converted input' do
          expect(including_class.new.calculate(input))
            .to eq result_for_valid_input
        end
      end
      context 'if input conversion fails' do
        it 'returns an informative message' do
          expect(including_class.new.calculate(invalid_input_from_unit))
            .to eq fail_conversion_message
        end
      end
    end

    describe '#normalize' do
      context 'if the input format is valid' do
        it 'normalize it by returning a converted input object' do
          expect(including_class.new.normalize(input)).to_not be_nil
        end
      end
      context 'if input not valid' do
        it 'returns nil' do
          expect(including_class.new.normalize(invalid_input_length)).to be_nil
          expect(including_class.new.normalize(invalid_input_no_number)).to be_nil
          expect(including_class.new.normalize(invalid_input_from_unit)).to be_nil
          expect(including_class.new.normalize(invalid_input_to_unit)).to be_nil
        end
      end
    end

    describe '#valid' do
      let(:splitted_input) { input.split " " }
      let(:number) { splitted_input[0] }
      let(:from) { splitted_input[1] }
      let(:to) { splitted_input[-1] }
      it 'returns true if input format is valid' do
        expect(including_class.new.valid?(number, from, to)).to be_truthy
      end
    end
  end
end
