require 'rails_helper'

RSpec.describe ConvertorsController, type: :controller do

  describe 'GET #status' do
    it 'Should return the current player' do
      get :status
      expect(response.body).to include_json({status: 200}.to_json)
    end
  end
end
