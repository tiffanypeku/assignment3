require "rails_helper"

RSpec.describe ConvertorsController, type: :routing do
  describe "routing" do

    it "routes to #convert" do
      expect(:get => "/convertors/convert").to route_to("convertors#convert", :format => :json)
    end

    it "routes to #status" do
      expect(:get => "/convertors/status").to route_to("convertors#status", :format => :json)
    end

  end
end
