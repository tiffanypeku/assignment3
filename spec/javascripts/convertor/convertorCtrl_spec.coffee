describe 'Convertor example', ->
  beforeEach module 'app.convertorApp'
  ConvertorCtrl = undefined
  scope = undefined
  notTakenCell = '-'
  takenCell = 'X'
  beforeEach inject(($rootScope, $controller) ->
    scope = $rootScope.$new()
    ConvertorCtrl = $controller 'ConvertorCtrl', $scope: scope
  )
  it 'true if already chosen by player, false otherwise', ->
    expect(scope.isTaken(takenCell)).toBeTruthy
    expect(scope.isTaken(notTakenCell)).toBeFalsy
