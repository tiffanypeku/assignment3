module Util
  def calculate(input)
    result = normalize(input)
    if result
      result.value
    else
      FAIL_CONVERSION_MESSAGE
    end
  end

  def normalize(input)
    splitted_in = input.split ' '
    return unless (splitted_in.count == 4) && (splitted_in.include? 'to')
    from = splitted_in[1].to_sym
    to   = splitted_in[-1].to_sym
    return unless valid?(splitted_in[0], from, to)
    Float(splitted_in[0])&.send(from)&.to&.send(to)
  end

  def valid?(string_number, from, to)
    number = Float(string_number)
    (number.try(from)) && (number.try(to))
  rescue
    return false
  end
end
