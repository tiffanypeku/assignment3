Rails.application.routes.draw do
  mount JasmineRails::Engine => '/specs' if defined?(JasmineRails)
  resources :convertors, only: [:index, :convert, :status] do
    collection do
      get 'convert', defaults: {format: :json}
      get 'status', defaults: {format: :json}
    end
  end
  root 'convertors#index'
end
