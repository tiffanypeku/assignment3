
@app = angular.module 'app.convertorApp', [ 'ngRoute', 'templates' ]

@app.config [ '$routeProvider', '$locationProvider', '$httpProvider',
   ($routeProvider, $locationProvider, $httpProvider)->
    $httpProvider.defaults.headers.common['X-CSRF-Token'] =
      $('meta[name=csrf-token]').attr('content')
    $routeProvider
      .when '/',
        templateUrl: 'convertors/index.html'
        controller : 'ConvertorCtrl'
      .otherwise
        redirectTo: '/'
    $locationProvider.html5Mode true
]

@app.run(->
  console.log 'angular app running'
)
