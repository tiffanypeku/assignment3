angular.module('app.convertorApp').controller("ConvertorCtrl", [
  '$scope', '$http'
  ($scope, $http)->
    console.log 'ConvertorCtrl running'

    #Get from API the defaults
    setup = ->
      $http.get('/convertors/status.json').success((data) ->
        unit  = data.unit
        value = data.value
        from  = data.from
        to    = data.to
        $scope.inputunit = "#{value} #{from} to #{to}"
        console.log('Successfully loaded status.')
      ).error (data) ->
        console.error('Failed to load status.')
    setup()

    $scope.submito = (cell) ->
      $http.get("/convertors/convert",{params:{"input": cell}}).success((response) ->
        $scope.conversion_result = response.result
      ).error (response) ->
])
