class ConvertorsController < ApplicationController

  def index
  end

  def convert
    response = { result: calculate(params[:input]), meta: {status: 200} }
    render :json => response.as_json
  end

  def status
    response = { unit: 'temperature', value: '1', from: 'celsius', to: 'fahrenheit', meta: {status: 200} }
    render :json => response.as_json
  end

end
