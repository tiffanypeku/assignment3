require 'alchemist'
require 'util/unit_processor'
class ApplicationController < ActionController::Base
  include Util
  protect_from_forgery with: :exception
end
