# Instructions
- bundle install
- rails s

Then go to localhost:3000

# Note
For the front-end, AngularJS has been used. The rails views are only
used to load JS and they don't contain anything client-side rendering.
